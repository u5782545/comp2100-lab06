package anu.cecs.comp2100.comp2100_android_testing;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    EditText numberEntryField;
    Button identityButton;
    TextView identityShow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        numberEntryField = (EditText) findViewById(R.id.editText);
        identityButton = (Button) findViewById(R.id.button);
        identityShow = (TextView) findViewById(R.id.textView);
    }

    /*private int identity(int x) {
        if (20 <= x && x <= 30) {
            x /= 2;
        }
        if (5 <= x && x <= 15) {
            x *= 2;
        }
        return x;
    }*/

    public int identity(int x) {
        if (20 <= x && x <= 30) {
            x /= 2;
        }
        if (5 <= x && x <= 15) {
            x *= 2;
        }
        return x;
    }

    public void showIdentity(View view){
        int number = Integer.parseInt(numberEntryField.getText().toString());
        identityShow.setText(Integer.toString(identity(number)));
    }

}
