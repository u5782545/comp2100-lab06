package anu.cecs.comp2100.comp2100_android_testing;

import android.test.ActivityInstrumentationTestCase2;
import android.test.suitebuilder.annotation.SmallTest;

/**
 * Created by Zhaolian on 18/04/2016.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity>{
        MainActivity mainActivity;

        public MainActivityTest(){
            super(MainActivity.class);
        }

        @Override
        protected void setUp() throws Exception{
            super.setUp();
            mainActivity = getActivity();
        }
        @SmallTest
        public void testIdentify(){
            int testData[] = {-100, -49, -10, -5, -1, 0, 1, 2, 3, 5, 6, 7, 8, 9, 10, 15, 17, 20, 23, 25, 30, 50, 100, 150};
            for (int input: testData){
                int output = mainActivity.identity(input);
                assertEquals(input,output);
            }
        }
    }
