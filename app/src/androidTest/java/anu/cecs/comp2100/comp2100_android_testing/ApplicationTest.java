package anu.cecs.comp2100.comp2100_android_testing;

import android.app.Application;
import android.test.ApplicationTestCase;
import android.test.suitebuilder.annotation.SmallTest;

/**
 * <a href="http://d.android.com/tools/testing/testing_android.html">Testing Fundamentals</a>
 */
public class ApplicationTest extends ApplicationTestCase<Application> {

    @Override
    protected void setUp() throws Exception{
        super.setUp();
    }

    public ApplicationTest() {
        super(Application.class);
    }
}